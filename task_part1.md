*- А долго мне придется здесь прожить, сударыня? - спросила Элли, опустив голову.*

*- Не знаю, - ответила Виллина. - Об этом ничего не сказано в моей волшебной книге. Иди, ищи, борись! Я буду время от времени заглядывать в волшебную книгу, чтобы знать, как идут твои дела… Прощай, моя дорогая!*

Уважаемый сотрудник!

Мы знаем, что Вы сейчас в отпуске, но мы убедительно просим Вас немедленно приступить к работе удалённо.

На выходных администратор офисного центра перепутала номера офисов и по ошибке пустила грузчиков в наш офис вместо соседнего. Грузчики отключили нашу оргтехнику и увезли её. Мы связались с транспортной компанией, чтобы нам вернули компьютеры и нам их вернут, но… компьютеры уже прошли таможенное оформление, загружены в контейнер, погружены на корабль и отправлены в Сидней. Компьютеры вернутся, самое раннее, через два месяца, а работать нам нужно уже сейчас.  

К счастью, наш сервер был заперт в коммутационном шкафу и его не тронули, также остались несколько компьютеров и маршрутизаторов в кладовке. Наша секретарь, Юлия Аркадьевна, соединила что смогла какими-то проводами, включила и теперь дело за Вами.

Вы получите доступ к экранам двух компьютеров «L0» и «L1» через броузер. Все настройки производите с этих компьютеров.







| **Компьютер «L0»** |    |
| --- | --- |
|На компьютере **«L0»** уже установлена операционная система (Calculate Linux XFCE (cldx)).||
|На компьютере уже есть пользователи:||
|`	` * guest с паролем «guest»||
|`	` * root с паролем «toor»||
|Назовите этот компьютер «secretary»|14+|
|Настройте сеть, так, чтобы компьютер смог выходить в интернет. Настройки сети определите самостоятельно.|14+|
|Установите следующее ПО:||
|` ` * офисный пакет Libre Office; графический редактор Gimp; Inkscape; видеоплеер vlc; аудиоплеер audacious|10+|
|` ` * борузеры Mozilla Firefox и Chromium |10+|
|`	` * с дополнением Adblock Plus|14+|
|` ` * hugin|14+|
|` ` * tmux версии 3.2 | 16+|
|Удалите установочные пакеты|14+|
|Удалите: аудио плеер установленный в системе по-умолчанию; программу для скачивания торрентов; программу gparted|10+|
|Создайте пользователя jular с основной группой «jular»|10+|
|Задайте пользователю jular пароль «123»|14+|
|`	` * а если не получится, то пароль: «D$tvGhbdtn»|10+|
|Добавьте пользователя jular в группу secretary|10+|
|Пользователь jular должен входить в графическую оболочку по умолчанию, без ввода пароля.|16+|
|У пользователя jular должны быть доступны 4 раскладки: русская, английская, немецкая и французская.|10+|
|Переключение между раскладками, клавишей CapsLock|10+|
|Настройте дисплей так, чтобы:||
|`	` * он гас через 3 минуты,  если за компьютером не работают|10+|
|`	` * отключался через 4 минуты, если за компьютером не работают|10+|
|`	` * при включении дисплея не требовался пароль|10+|
| Создайте папку /opt/exchange|10+|
| В ней подпапки: docs, pub, engeneers, secretary, restricted|10+|
| Создайте следующих пользователей со следующими паролями:	||
|`	` * zvv		«V0qrhenjqg@hjkm1»||
|`	` * ova		«V0qrhenjqg@hjkm2»||
|`	` * mgt		«V0qrhenjqg@hjkm3»||
|`	` * saa		«V0qrhenjqg@hjkm4»||
|`	` * lautre		«V0qrhenjqg@hjkm5»||
|`	` * chekaniu	«V0qrhenjqg@hjkm6»|10+|
| у каждого из этих пользователей должна быть создана домашняя папка и основная группа, по названию совпадающая с именем пользователя.|10+|
| Создайте следующие группы:||
|`	` * users, добавьте в неё всех ранее созданных пользователей|10+|
|`	` * engeneers, добавьте в нее пользователей ova, mgt|10+|
|`	` * geeks, добавьте в неё пользователей saa и zvv|10+|
|`	` * guests, добавьте в неё пользователей lautre и chekaniu|10+|
|Задайте на папки следующие права:||
|`	` * pub — у всех есть права на запись и чтение в этой папке|14+|
|`	` * engeneers — члены группы engeneers имеют полные права на папку, остальные только на просмотр содержимого папки и чтение файлов в ней.|14+|
|`	` * secretary — полный доступ к папке у группы secretary, остальные доступа в папку не имеют.|14+|
|`	` * docs — полный доступ у группы secretary, у остальных доступ на чтение и просмотр.</p>|14+|
|`	` * restricted — полный доступ для geeks; доступ на чтение и просмотр у группы engeneers, остальные доступа в папку не имеют.</p>|16+|


|**Компьютер «L1»**|   |
| --- | --- |
|В USB порту компьютера **«L1»** установлен загрузочный flash накопитель. C дистрибутивом «Simle Linux 9.1».||Разбейте жесткий диск на три основных раздела:||
|` ` 1.	/boot	ext2	512Mb||
|` ` 2. swap	swap	512Mb||
|` ` 3.	/	ext4	всё оставшееся пространство|14+|
|Установите на компьютер операционную систему|10+|


|**Компьютер «L0»**| |
| --- | --- |
|В USB порту компьютера **«L0»** установлен flash накопитель. На нём была полезная информация, но теперь она потеряна. Попробуйте восстановить данные с диска.|14+|
|Найдите оставшуюся часть задания среди восстановленных файлов.|14+|

